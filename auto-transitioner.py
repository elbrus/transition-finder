#!/usr/bin/python3

import apt_pkg
import os
import sys

from debian.rt.package import (SourcePackage, BinaryPackage)
from debian.rt.mirror import PackageMirrorDist
from debian.rt.util import (binary_external_rdeps, binary_rdeps,
                            compute_reverse_dependencies,
                            read_sources, read_binaries
                           )


def transitions(src_test, bin_test, src_new):
    '''Find source packages with are part of a transition

    Source packages are part of a transition if they have removed binary
    packages with reverse dependencies in testing. Removal transitions
    are not considered.
    '''

    for source in sorted(src_test):
        transition_name = source
        test_bin = src_test[source].binaries
        if source in src_new:
            new_suite_bin = src_new[source].binaries
        else:
            # Potential removal transition, I don't care
            continue

        if test_bin <= new_suite_bin:
            # Only binaries are added
            continue

        old_bin = sorted(x for x in test_bin - new_suite_bin)

        yield (transition_name, source, old_bin)


if __name__ == "__main__":
    apt_pkg.init()

    mirror_test = PackageMirrorDist(sys.argv[1])
    mirror_sid = PackageMirrorDist(sys.argv[2])

    src_test = read_sources(mirror_test)
    src_sid = read_sources(mirror_sid)

    bin_test = read_binaries(mirror_test)

    compute_reverse_dependencies(bin_test)

    possible_transitions = list(transitions(src_test, bin_test, src_sid))

    if not possible_transitions:
        exit(0)

    bin_sid = read_binaries(mirror_sid)

    compute_reverse_dependencies(bin_sid)

    rdeps_bin = set()
    rdeps_src = set()

    for transition_name, source, old_binaries in possible_transitions:
        for binary in old_binaries:
            (src_rdeps_bin, src_rdeps_src) = binary_external_rdeps(source, binary, bin_sid)
            new_rdeps = src_rdeps_bin - rdeps_bin
            rdeps_bin.update(new_rdeps)
            rdeps_src.update(src_rdeps_src)
            while True:
                try:
                    pkg = new_rdeps.pop()
                    (pkg_rdeps_bin, pkg_rdeps_src) = binary_rdeps(pkg, bin_sid)
                    new_rdeps.update(pkg_rdeps_bin - rdeps_bin)
                    rdeps_bin.update(pkg_rdeps_bin)
                    rdeps_src.update(pkg_rdeps_src)
                except KeyError:
                    break

    print(rdeps_src)
