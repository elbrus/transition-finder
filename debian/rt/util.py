import apt_pkg
import re
import sys

from debian.rt.package import (SourcePackage, BinaryPackage)


def binary_external_rdeps(source, binary, bin_suite):
    rdeps_bin = set()
    rdeps_src = set()
    if binary in bin_suite:
        for rdep in bin_suite[binary].reverse_depends:
            if rdep.source != source and rdep.architecture != 'all':
                rdeps_src.add(rdep.source)
                rdeps_bin.add(rdep.package)
    return (rdeps_bin, rdeps_src)


def binary_rdeps(binary, bin_suite):
    rdeps_bin = set()
    rdeps_src = set()
    if binary in bin_suite:
        for rdep in bin_suite[binary].reverse_depends:
            if rdep.architecture != 'all':
                rdeps_src.add(rdep.source)
                rdeps_bin.add(rdep.package)
    return (rdeps_bin, rdeps_src)


def read_sources(mirror_dist, sources=None, intern=sys.intern):
    if sources is None:
        sources = {}

    for filename in mirror_dist.sources_files:
        tag_file = apt_pkg.TagFile(filename)
        get_field = tag_file.section.get
        step = tag_file.step

        while step():
            if get_field('Extra-Source-Only', 'no') == 'yes':
                # Ignore sources only referenced by Built-Using
                continue
            pkg = intern(get_field('Package'))
            ver = intern(get_field('Version'))

            if pkg in sources and apt_pkg.version_compare(sources[pkg].version, ver) > 0:
                continue

            binaries = frozenset(x.strip() for x in get_field('Binary').split(','))

            sources[pkg] = SourcePackage(
                source=pkg,
                package=pkg,
                version=ver,
                source_version=ver,
                binaries=binaries,
            )

    return sources

def read_binaries(mirror_dist, packages=None, intern=sys.intern):
    if packages is None:
        packages = {}

    for filename in mirror_dist.packages_files:
        tag_file = apt_pkg.TagFile(filename)
        get_field = tag_file.section.get
        step = tag_file.step

        while step():
            pkg = intern(get_field('Package'))
            version = intern(get_field('Version'))
            source = get_field('Source', pkg)
            source_version = version

            # There may be multiple versions of any arch:all packages
            # (in unstable) if some architectures have out-of-date
            # binaries.
            if pkg in packages and apt_pkg.version_compare(packages[pkg].version, version) > 0:
                continue

            if "(" in source:
                source, v = (x.strip() for x in source.split("("))
                v.rstrip(" )")
                source = intern(source)
                source_version = intern(v)

            section = intern(get_field('Section', 'N/A'))

            depends_field = get_field('Depends')
            predepends_field = get_field('Pre-Depends')
            depends = []
            # When processing multiple architectures, the list
            # of dependencies for a given binary package may
            # differ between architectures. Rather than simply
            # using the dependencies from the first or last
            # instance of the package encountered, we instead
            # combine them into a single list generated across
            # all of the architectures.
            if pkg in packages:
                depends = packages[pkg].depends
            if depends_field:
                depends.extend(apt_pkg.parse_depends(depends_field))
            if predepends_field:
                depends.extend(apt_pkg.parse_depends(predepends_field))
            # Remove duplicate entries from the dependency list.
            depends = [list(x) for x in set(tuple(x) for x in depends)]

            bin_pkg = BinaryPackage(
                package=pkg,
                version=version,
                architecture=get_field('Architecture'),
                source=source,
                source_version=source_version,
                section=section,
                depends=depends,
                reverse_depends=set(),
            )

            packages[pkg] = bin_pkg


    return packages


def compute_reverse_dependencies(packages):
    for pkg_name in packages:
        pkg = packages[pkg_name]
        for ordep in pkg.depends:
            for dep in ordep:
                dep_pkg = dep[0]
                if dep_pkg not in packages:
                    continue
                packages[dep_pkg].reverse_depends.add(pkg)
    return
